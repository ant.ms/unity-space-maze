﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class charging : MonoBehaviour
{

    public GameObject player;

    void OnCollisionEnter()
    {
        player.GetComponentInChildren<PlayerMovement>().fuel = player.GetComponentInChildren<PlayerMovement>().maxFuel;
    }
}
