﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public Slider fuelSlider;

    public float sidewaysForce = 500f;
    public float boostForce = 2000f;
    public float jumpForce = 300f;

    public float maxFuel = 20f;
    public float fuel;

    // Start is called before the first frame update
    void Start()
    {
        fuel = maxFuel;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // #region desktop
            // "jetpack"
            if (Input.GetKey("space")) {
                jump();
            }
            
            if (Input.GetKey("d")) {
                // extra speed
                if (Input.GetKey(KeyCode.LeftShift)) {
                    rb.AddForce(boostForce * Time.deltaTime, 0, 0);
                } else {
                    rb.AddForce(sidewaysForce * 5 * Time.deltaTime, 0, 0);
                }
            }
            if (Input.GetKey("a")) {
                // extra speed
                if (Input.GetKey(KeyCode.LeftShift)) {
                    rb.AddForce(-boostForce * Time.deltaTime, 0, 0);
                } else {
                    rb.AddForce(-sidewaysForce * 5 * Time.deltaTime, 0, 0);
                }
            }
        // #endregion

        // #region mobile
        Vector3 acc = Input.acceleration;
        rb.AddForce(acc.x * sidewaysForce, 0, 0);

        if (Input.touchCount == 1) {
            jump();
        }

        // #endregion

        fuelSlider.value = fuel / maxFuel;
    }

    void jump() {
        if (fuel > 0) {
            rb.AddForce(0, jumpForce, 0);
            if (fuel >= 1) fuel -= 1;
        }
    }
}
